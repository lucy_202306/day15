# day15-ORID

## O

1. code review. Add the global exception that did not complete yesterday, replace the DoneListPage interface, and handle the page error when getTodoById did not find the value.
2. Learned the concepts of BA, QA, UX and Dev, and divided project roles in the team.
3. Analyze the highlights of next week's project and give an elevator speech.
4. Learn how to draw the user journey and user story, and list the user journey and user story of the project.
5. Do group Retro

## R

confused

## I

1. I am not very familiar with the user journey and user story. 
2. When I added the page exception reminder for yesterday's job, I found that the page always had to be rendered twice. I did not find a solution.

## D

Prepare for next week's project.



